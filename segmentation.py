import skimage.io as skim
import os
from skimage.viewer import ImageViewer as view
from skimage.segmentation import slic
from matplotlib import pyplot as canvas

def segslic(imagename,path='OSU-Wound_images/',nseg=20,labspace=0):
	image=skim.imread(path+imagename);
	segment=slic(image, n_segments=nseg, convert2lab=labspace, enforce_connectivity=1 )
	return segment

def imshow(image):
	canvas.show(skim.imshow(image))

def imsave(imagename,image):
	canvas.imsave(imagename,image,format='jpg')

def autoslic(imagename,path='OSU-Wound_images/',outdir='result/',n=20,lab=0):
	result=segslic(imagename,path,n,lab);
	imsave(outdir+imagename,result)

def ls(path):
	list=os.listdir(path)
	return list
